# yonderbread/config

## Locations:

-   [Everything](src)
    -   [Misc Configs](src/misc)
    -   [Build Scripts](src/scripts/comp)
    -   [Game Configs](src/scripts/game)
    -   [Installation Scripts](src/scripts/inst)
    -   [Misc Scripts](src/scripts/misc)
    -   [Fonts/Font Families](src/fonts)
    -   [Custom Gitignores](src/gitignores)

## Commands

### Install NPM dependencies

```bash
npm i
```

### Lint for text file format inconsistencies

```bash
npm run lint
```

### Format all with prettier

```bash
npm run format
```
