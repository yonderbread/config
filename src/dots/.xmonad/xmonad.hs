import XMonad
import XMonad.Util.Run (runProcessWithInput, safeSpawn, spawnPipe)
import XMonad.Util.SpawnOnce
import XMonad.Util.EZConfig
import XMonad.Util.Ungrab
import XMonad.Hooks.ManageDocks (avoidStruts, docks, ToggleStruts (ToggleStruts), docksStartupHook)
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.EwmhDesktops as E
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.StatusBar
import XMonad.Hooks.StatusBar.PP
import XMonad.Layout.Fullscreen
import XMonad.Layout.NoBorders
import XMonad.Layout.Spiral
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns
import XMonad.Layout.Spacing
import XMonad.Config.Xfce
import Data.Monoid
import System.Exit

import qualified XMonad.StackSet as W
import qualified Data.Map as M

myTerminal              = "alacritty"
myBrowser               = "librewolf"
myFileExplorer          = "pcmanfm"
myVideoPlayer           = "mpv-launch"
myTextEditor            = "gedit"
myPolkitAgent           = "lxpolkit"
myFocusFollowsMouse    :: Bool
myFocusFollowsMouse     = True
myClickJustFocuses     :: Bool
myClickJustFocuses      = True
myModMask               = mod4Mask
myWorkspaces            = ["1","2","3","4","5","6","7","8","9"]
myBorderWidth           = 1
myNormalBorderColor     = "#dddddd"
myFocusedBorderColor    = "#80c0ff"

-- Keybinds
myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $
    [ 
      -- launch terminal
      ((modm, xK_Return), spawn $ myTerminal),
      -- launch file explorer
      ((modm .|. shiftMask, xK_Return), spawn $ myFileExplorer),
      -- launch text editor
      ((modm .|. shiftMask, xK_g), spawn $ myTextEditor),
      -- launch browser
      ((modm .|. shiftMask, xK_b), spawn $ myBrowser),
      -- launch video player
      ((modm .|. shiftMask, xK_m), spawn $ myVideoPlayer),
      -- launch dmenu
      ((modm, xK_d), spawn $ "dmenu_run"),
      -- open youtube
      ((modm .|. shiftMask, xK_y), spawn $ "brave-browser https://youtube.com"),
      -- launch gmrun
      --((modm .|. shiftMask, xK_p), spawn "gmrun"),
      -- launch rofi
      ((modm .|. shiftMask, xK_p), spawn "rofi -theme ~/.config/rofi/themes/ayu.rasi -show drun -show-icons -icon-theme \"Dracula\""),
      -- run splash wallpaper grabber
      ((modm .|. shiftMask, xK_s), spawn "splash -c"),
      -- launch brave browser
      ((modm, xK_b), spawn "brave-browser"),
      -- launch mouser
      ((modm .|. xK_question), spawn "mouser"),
      -- close focsed window
      ((modm .|. shiftMask, xK_q), kill),
      -- cycle available layouts
      ((modm, xK_space), sendMessage NextLayout),
      -- reset layout on current workspace
      ((modm .|. shiftMask, xK_space), setLayout $ XMonad.layoutHook conf),
      -- resize window automatically
      ((modm, xK_n), refresh),
      -- move focus to next window
      ((modm, xK_Tab), windows W.focusDown),
      ((modm, xK_j), windows W.focusDown),
      ((modm, xK_Right), windows W.focusDown),
      -- move focus to previous window
      ((modm, xK_k), windows W.focusUp),
      ((modm, xK_Left), windows W.focusUp),
      -- move focus to master window 
      ((modm, xK_m), windows W.focusMaster),
      ((modm, xK_Up), windows W.focusMaster),
      -- swap focused window and master window
      --((modm, xK_Return), windows W.swapMaster),
      ((modm, xK_Down), windows W.swapMaster),
      -- swap focused window with next window
      ((modm .|. shiftMask, xK_j), windows W.swapDown),
      ((modm .|. shiftMask, xK_Right), windows W.swapDown),
      -- swap focused window with previous window
      ((modm .|. shiftMask, xK_k), windows W.swapUp),
      ((modm .|. shiftMask, xK_Left), windows W.swapUp),
      -- shrink master area
      ((modm, xK_h), sendMessage Shrink),
      -- expand master area
      ((modm, xK_l), sendMessage Expand),
      -- push window back into tiling
      ((modm, xK_t), withFocused $ windows . W.sink),
      -- add window to master area
      ((modm, xK_comma), sendMessage (IncMasterN 1)),
      -- remove window from master area
      ((modm, xK_period), sendMessage (IncMasterN (-1))),
      -- lock screen (i3lock-fancy)
      ((modm .|. shiftMask, xK_l), spawn "i3lock-fancy"),
      -- launch screenshot tool (flameshot)
      ((modm .|. shiftMask, xK_o), spawn "flameshot gui"),
      -- quit xmonad
      ((modm .|. shiftMask, xK_Escape), io (exitWith ExitSuccess)),
      -- restart xmonad
      ((modm .|. shiftMask, xK_c), spawn "xmonad --recompile; xmonad --replace; xmonad --restart")
    ]
    ++
    -- Switch to workspace N: {MODKEY}-{1..9}
    -- Move client to workspace N: {MODKEY}-Shift-{1..9}
    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9],
        (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    ++
    -- Switch screen: {MODKEY}-{w,e,r}
    -- Move client to screen: {MODKEY}-Shift-{w,e,r}
    [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..],
        (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]

-- Mouse bindings
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $
    -- mod-button1, Set window to floating and drag to move
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w >> windows W.shiftMaster)),
    -- mod-button2, Raise the window to top of stack
      ((modm, button2), (\w -> focus w >> windows W.shiftMaster)),
    -- mod-button3, Set window to floating and drag to resize
      ((modm, button3), (\w -> focus w >> mouseResizeWindow w >> windows W.shiftMaster))
    -- NOTE : Other buttons for scroll wheel exist such button4 and button5
    ]

-- Layouts
-- (Use {MODKEY}-Shift-Backspace after restarting xmonad to reset your layout state to new defaults)
myLayout = avoidStruts (
    spacing 5 (smartBorders (ThreeColMid 1 (3/100) (1/2)))    |||
    spacing 5 (smartBorders (ThreeColMid 1 (3/100) (1/2)) )   |||
    spacing 5 (smartBorders (Tall 1 (3/100) (1/2)) )          |||
    spacing 5 (smartBorders (Mirror (Tall 1 (3/100) (1/2))) ) |||
    spacing 5 (smartBorders (tabbed shrinkText tabConfig) )   |||
    spacing 5 (smartBorders(Full))
    ) -- |||
--    noBorders (fullscreenFull Full)

-- Colors for text and backgrounds of each tab when in "Tabbed" layout.
tabConfig = def {
    activeBorderColor = "#7C7C7C",
    activeTextColor = "#CEFFAC",
    activeColor = "#000000",
    inactiveBorderColor = "#7C7C7C",
    inactiveTextColor = "#EEEEEE",
    inactiveColor = "#000000"
}

-- Window rules
myManageHook = composeAll
    [ resource =? "desktop_window"   --> doIgnore,
      resource =? "kdesktop"         --> doIgnore,
      resource =? "flameshot"        --> doFloat,
      --className =? "Steam"           --> doFloat, 
      resource =? "xmessage"         --> doFloat ]

-- Status bars & logging
--myLogHook = return ()
-- Startup hook
myStartupHook :: X ()
myStartupHook = do
--    spawnOnce "lxsession &"
    spawnOnce "xrandr --output HDMI-0 --mode 2560x1440 --rate 144 --left-of DVI-D-0 --primary"
    spawnOnce "xrandr --output DVI-D-0 --mode 1920x1080 --rate 60 --right-of HDMI-0"
    spawnOnce "xrdb -merge ~/.Xresources"
    --spawnOnce "compton -b --config ~/.compton.conf &"
    spawnOnce "picom --config ~/.picom.simple.conf &"
    --spawnOnce "nm-applet &"
    spawnOnce "nitrogen --restore &"
    spawnOnce "ckb-next -b -c &"
    --spawnOnce "xmobar ~/.config/xmobar/xmobarrc &"
    --spawnOnce "exec $HOME/.config/lemonbar/run.sh"
    spawnOnce $ "xfce4-session &"

-- Run XMonad with configuration
main :: IO()
main = do 
    xmonad . ewmhFullscreen . ewmh
    . withEasySB (statusBarProp "cd ~/.config/lemonbar && ./run.sh" (pure def)) 
    defToggleStrutsKey $ def {
    -- basic
    terminal            = myTerminal,
    focusFollowsMouse   = myFocusFollowsMouse,
    clickJustFocuses    = myClickJustFocuses,
    borderWidth         = myBorderWidth,
    modMask             = myModMask,
    workspaces          = myWorkspaces,
    normalBorderColor   = myNormalBorderColor,
    focusedBorderColor  = myFocusedBorderColor,
    -- keybinds
    keys                = myKeys,
    -- hooks & layouts
    layoutHook          = myLayout,
    manageHook          = myManageHook,
    handleEventHook     = handleEventHook def <+> E.fullscreenEventHook,
    startupHook         = myStartupHook
    }
