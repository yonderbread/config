import subprocess as subp
import os, json
torproxy_url = 'socks://localhost:9050'
downloadsdir = '~/Downloads'
config.load_autoconfig(False)
c.confirm_quit = ['downloads']
c.auto_save.interval = 5000
c.changelog_after_upgrade = "patch"
c.content.autoplay = False
c.content.cookies.accept = 'no-unknown-3rdparty'
config.set('content.cookies.accept', 'all', 'chrome-devtools://*')
config.set('content.cookies.accept', 'all', 'devtools://*')
c.content.cookies.store = True
#c.content.headers.user_agent = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2919.83'
c.content.headers.user_agent= 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2919.83 Safari/537.36'
c.content.blocking.method = 'both'
with open(os.path.expanduser('~/.config/qutebrowser/adblocklists.json'),'r') as adbl:
    adblocklists = json.load(adbl)
    c.content.blocking.adblock.lists = adblocklists
c.content.images = True
c.content.javascript.enabled = True
c.content.notifications.enabled = False
c.content.private_browsing = False
c.tabs.last_close = 'default-page'
c.content.proxy = 'none'
c.completion.height = '43%'
c.completion.quick = True
c.completion.show = 'always'
c.completion.shrink = True
c.downloads.location.directory = downloadsdir
c.downloads.location.prompt = True
c.editor.command = ['nvim', '{file}']
c.input.insert_mode.auto_load = True
c.input.insert_mode.auto_leave = True
c.scrolling.bar = 'always'
c.scrolling.smooth = True
c.tabs.background = True
c.tabs.select_on_remove = 'last-used'
c.tabs.min_width = -1
c.zoom.default = '100%'
c.url.default_page = '~/.config/qutebrowser/index.html'
searchengines = {
    'sx': 'https://searx.be/search?q={}',
    'sx2': 'https://swag.pw/search?q={}',
    'sxt': 'http://w5rl6wsd7mzj4bdkbuqvzidet5osdsm5jhg2f7nvfidakfq5exda5wid.onion/search?q={}&categories=general',
    'br': 'https://search.brave.com/search?q={}&source=web',
    'd2gtor': 'https://duckduckgogg42xjoc72x3sjasowoarfbgcmvfimaftt6twagswzczad.onion/{}'}
searchengines["DEFAULT"] = searchengines.get("br")
c.url.searchengines = searchengines
c.url.start_pages = '~/.config/qutebrowser/index.html'
c.fonts.default_family = 'Iosevka Nerd Font Mono'
def setQutebrowserTheme(c, theme = 'dracula', options = {}):
    palette = options[theme].get("palette")
    spacing = options[theme].get("spacing")
    padding = options[theme].get("padding")
    c.colors.completion.category.bg = palette['background']
    c.colors.completion.category.border.bottom = palette['border']
    c.colors.completion.category.border.top = palette['border']
    c.colors.completion.category.fg = palette['foreground']
    c.colors.completion.even.bg = palette['background']
    c.colors.completion.odd.bg = palette['background-alt']
    c.colors.completion.fg = palette['foreground']
    c.colors.completion.item.selected.bg = palette['selection']
    c.colors.completion.item.selected.border.bottom = palette['selection']
    c.colors.completion.item.selected.border.top = palette['selection']
    c.colors.completion.item.selected.fg = palette['foreground']
    c.colors.completion.match.fg = palette['orange']
    c.colors.completion.scrollbar.bg = palette['background']
    c.colors.completion.scrollbar.fg = palette['foreground']
    c.colors.downloads.bar.bg = palette['background']
    c.colors.downloads.error.bg = palette['background']
    c.colors.downloads.error.fg = palette['red']
    c.colors.downloads.stop.bg = palette['background']
    c.colors.downloads.system.bg = 'none'
    c.colors.hints.bg = palette['background']
    c.colors.hints.fg = palette['purple']
    c.hints.border = '1px solid ' + palette['background-alt']
    c.colors.hints.match.fg = palette['foreground-alt']
    c.colors.keyhint.bg = palette['background']
    c.colors.keyhint.fg = palette['purple']
    c.colors.keyhint.suffix.fg = palette['selection']
    c.colors.messages.error.bg = palette['background']
    c.colors.messages.error.border = palette['background-alt']
    c.colors.messages.error.fg = palette['red']
    c.colors.messages.info.bg = palette['background']
    c.colors.messages.info.border = palette['background-alt']
    c.colors.messages.info.fg = palette['comment']
    c.colors.messages.warning.bg = palette['background']
    c.colors.messages.warning.border = palette['background-alt']
    c.colors.messages.warning.fg = palette['red']
    c.colors.prompts.bg = palette['background']
    c.colors.prompts.border = '1px solid ' + palette['background-alt']
    c.colors.prompts.fg = palette['cyan']
    c.colors.prompts.selected.bg = palette['selection']
    c.colors.statusbar.caret.bg = palette['background']
    c.colors.statusbar.caret.fg = palette['orange']
    c.colors.statusbar.caret.selection.bg = palette['background']
    c.colors.statusbar.caret.selection.fg = palette['orange']
    c.colors.statusbar.command.bg = palette['background']
    c.colors.statusbar.command.fg = palette['pink']
    c.colors.statusbar.command.private.bg = palette['background']
    c.colors.statusbar.command.private.fg = palette['foreground-alt']
    c.colors.statusbar.insert.bg = palette['background-attention']
    c.colors.statusbar.insert.fg = palette['foreground-attention']
    c.colors.statusbar.normal.bg = palette['background']
    c.colors.statusbar.normal.fg = palette['foreground']
    c.colors.statusbar.passthrough.bg = palette['background']
    c.colors.statusbar.passthrough.fg = palette['orange']
    c.colors.statusbar.private.bg = palette['background-alt']
    c.colors.statusbar.private.fg = palette['foreground-alt']
    c.colors.statusbar.progress.bg = palette['background']
    c.colors.statusbar.url.error.fg = palette['red']
    c.colors.statusbar.url.fg = palette['foreground']
    c.colors.statusbar.url.hover.fg = palette['cyan']
    c.colors.statusbar.url.success.http.fg = palette['green']
    c.colors.statusbar.url.success.https.fg = palette['green']
    c.colors.statusbar.url.warn.fg = palette['yellow']
    c.statusbar.padding = padding
    c.colors.tabs.bar.bg = palette['selection']
    c.colors.tabs.even.bg = palette['selection']
    c.colors.tabs.even.fg = palette['foreground']
    c.colors.tabs.indicator.error = palette['red']
    c.colors.tabs.indicator.start = palette['orange']
    c.colors.tabs.indicator.stop = palette['green']
    c.colors.tabs.indicator.system = 'none'
    c.colors.tabs.odd.bg = palette['selection']
    c.colors.tabs.odd.fg = palette['foreground']
    c.colors.tabs.selected.even.bg = palette['background']
    c.colors.tabs.selected.even.fg = palette['foreground']
    c.colors.tabs.selected.odd.bg = palette['background']
    c.colors.tabs.selected.odd.fg = palette['foreground']
    c.tabs.padding = padding
    c.tabs.indicator.width = 1
    c.tabs.favicons.scale = 1
with open(os.path.expanduser(
'~/.config/qutebrowser/qb.theme.json'), 'r') as _th:
	_opts = json.load(_th)
	setQutebrowserTheme(c=c, 
		options=_opts,
		theme="substrata")
c.fonts.default_size = '12pt'
c.content.user_stylesheets = [
    "~/.config/qutebrowser/stylesheets/github.com.css",
    "~/.config/qutebrowser/stylesheets/misc.css",
    "~/.config/qutebrowser/stylesheets/reddit.com.css",
    "~/.config/qutebrowser/stylesheets/scrollbar.css",
    "~/.config/qutebrowser/stylesheets/stackoverflow.com.css",
    "~/.config/qutebrowser/stylesheets/youtube.com.css",
    "~/.config/qutebrowser/stylesheets/twitter.com.css",
    "~/.config/qutebrowser/stylesheets/wiki.archlinux.org.css",
    "~/.config/qutebrowser/stylesheets/ultimate-guitar.com.css"
    ]
mybindings = {
    ',': 'spawn mpv {url}',
    'l': 'spawn ' + \
        'mkdir -p ~/Downloads/qutebrowser-ytdl && ' + \
        'youtube-dl -f best -ci ' + \
        '-o "~/Downloads/qutebrowser-ytdl/%(title)s.%(ext)s"' + \
        ' {url}',
    'X': 'tab-close',
    'Z-': 'tab-prev',
    'Z+': 'tab-next',
    'Z,': 'bookmark-add'
}
# Configure settings and Qutebrowser proxy client
# if the tor service is running 
if subp.run(["ps", "-C", "tor"]).returncode == 0:
    c.content.proxy = torproxy_url
    c.content.cookies.accept = 'never'
    c.content.cookies.store = False
    c.content.private_browsing = True
    c.content.javascript.enabled = False
    c.confirm_quit = ['never']
    c.tabs.last_close = 'blank'
    c.url.searchengines['DEFAULT'] = c.url.searchengines['sxt']
    c.url.default_page = 'about:blank'
    c.url.start_pages = 'about:blank'
