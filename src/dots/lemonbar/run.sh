#!/usr/bin/bash
pkill lemonbar
./config1.sh | lemonbar -d -p -g "2560x22+0+0" \
    -o "HDMI-0" \
    -f "Mononoki Nerd Font" \
    -f "FontAwesome" \
    -B "#191C25" \
    -F "#C6AED7" \
    -U "#2E313D" \
    -u 2 &

./config2.sh | lemonbar -d -p -g "1920x22+2560+0" \
    -o "DVI-D-0" \
    -f "Mononoki Nerd Font" \
    -f "FontAwesome" \
    -B "#191C25" \
    -F "#C7AED7" \
    -U "#2E313D" \
    -u 2 &
