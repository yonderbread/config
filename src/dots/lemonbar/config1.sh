#!/usr/bin/bash

SEP_ICON_RIGHT="\uf053"
SEP_ICON_LEFT="\uf054"

GetLayout()
{
    python ~/.config/lemonbar/scripts/query.py screenlayout 0
}

GetGroup()
{
    python ~/.config/lemonbar/scripts/query.py screengroup 0
}

GetVolume() 
{
    VOLUMEPERC=$(pulseaudio-control output | cut -d ' ' -f1)
    echo "$VOLUMEPERC"
}

GetTime() 
{
    DATE_TIME=$(date "+%T")
    echo -n "$DATE_TIME"
}

GetDate() 
{
    DATE_DATE=$(date "+%D")
    echo -n "$DATE_DATE"
}

GetMemory() 
{
    #MEMUSEDGB=$(free | grep Mem | awk '{ printf("%.4f %\n", $4/$2 * 100.0) }' | cut -b -4)
    MEMUSED=$(free -m| grep  Mem | awk '{ print int($3/$2*100) }')
    echo "$MEMUSED"
}

GetWindowTitle() 
{
    WIN_TITLE=$(xdotool getactivewindow getwindowname)
    echo $WIN_TITLE
}

GetCpuUsage() 
{
    CPU_USAGE=$(bash ~/.config/lemonbar/scripts/cpu.sh)
    echo $CPU_USAGE
}

GetCpuAvgTemp() 
{
    CPU_TEMP=$(python ~/.config/lemonbar/scripts/get_avg_cpu_temp.py)
    echo $CPU_TEMP
}

while true; do 
    echo -e "%{l}%{F#5B5F71}$(GetWindowTitle)%{F-}%{F#B5B4C9} [%{F-}%{F#FE9F7C}$(GetGroup)%{F-}%{F#B5B4C9}] [%{F-}%{F#659EA2}$(GetLayout)%{F-}%{F#B5B4C9}] ${SEP_ICON_LEFT}%{F-}%{r}" \
    	"%{F#B5B4C9}/%{F-}" \
        "cpu:$(GetCpuUsage)% %{F#B5B4C9}/ %{F-}temp:$(GetCpuAvgTemp)C %{F#B5B4C9}/%{F-}"\
        "memused:$(GetMemory)% %{F#B5B4C9}/ %{F-}date:$(GetDate)%{F#B5B4C9} / %{F-}time:$(GetTime)"\
        "%{F#B5B4C9}/ %{F-}vol:$(GetVolume) %{F-}%{B-}"
    sleep 1;
done
