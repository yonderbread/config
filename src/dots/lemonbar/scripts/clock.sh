#!/usr/bin/env bash
tr '[:lower:]' '[:upper:]' <<< $(date "+%I:%M %p")
