#!/usr/bin/env python
from libqtile.command.client import CommandClient
import sys

if __name__ == "__main__":
    c = CommandClient()
    screens = c.call("screens")
    groups = c.call("groups")
    _args = sys.argv
    if len(_args) >= 3:
        if _args[1].lower() == "group":
            sys.stdout \
                .write(screens[int(_args[2])]["group"])
            sys.exit(0)
        elif _args[1].lower() == "layout":
            sys.stdout.write(groups[_args[2]]["layout"])
            sys.exit(0)
        elif _args[1].lower() == "layouts":
            sys.stdout.writelines(
                [].append(
                    v["layout"] for v in groups.values()
                )
            )
            sys.exit(0)
        elif _args[1].lower() == "screengroup":
            sys.stdout \
            .write \
            (groups[screens[int(_args[2])]['group']]['label'])
            sys.exit(0)
        elif _args[1].lower() == "screenlayout":
            sys.stdout \
            .write \
            (groups[screens[int(_args[2])]['group']]['layout'])
            sys.exit(0)
    sys.stdout.write("Error!")
    sys.exit(1)
