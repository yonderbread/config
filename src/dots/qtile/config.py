from typing import List

from libqtile import bar, layout, widget
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile import hook

from myqtile.keybinds import keys
from myqtile.globals import (
    modKey,
    FONT_PRIMARY,
    colorpal,
    myTerminal,
    myEditor,
    myBrowser,
    myPlayer)
from myqtile.layouts import layouts
import subprocess

@hook.subscribe.startup_once
def autostart():
    subprocess.call('~/.config/qtile/myqtile/autostart.sh', shell=True)

groups = [Group(i) for i in "123456789"]

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([modKey], i.name, lazy.group[i.name].toscreen(),
            desc="Switch to group {}".format(i.name)),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([modKey, "shift"], i.name, lazy.window.togroup(i.name, switch_group=False),
            desc="Switch to & move focused window to group {}".format(i.name)),
    ])

widget_defaults = dict(
    font=FONT_PRIMARY["font"],
    fontsize=FONT_PRIMARY["fontsize"],
    padding=FONT_PRIMARY["padding"],
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Gap(
            size=22
            )
        #        top=bar.Bar(
#            [
#                widget.CurrentLayout(),
#                widget.GroupBox(),
#                widget.Prompt(),
#                widget.WindowName(),
#                widget.Chord(
#                    chords_colors={
#                        'launch': (colorpal["backgroundalt"], colorpal["foregroundalt"]),
#                    },
#                    name_transform=lambda name: name.upper(),
#                ),
                #widget.TextBox(subprocess.check_output(["/bin/bash","-c","uname -r"]).decode('utf-8'), name="default"),
#                widget.Systray(),
#                widget.Clock(format='%Y-%m-%d %a %I:%M %p#'),
#                widget.Clock(format="[%m/%d/%Y %I:%M %p]")
#            ],
#            24,
#            background=colorpal["backgroundalt"],
#            opacity=0.88
#        ),
    ),
    Screen(
        top=bar.Gap(size=22),
#        top=bar.Bar(
#            [
#                widget.CurrentLayout(),
#                widget.GroupBox(),
#                widget.Prompt(),
#                widget.WindowName(),
#                widget.Chord(
#                    chords_colors={
#                        'launch': (colorpal["backgroundalt"], colorpal["foregroundalt"]),
#                    },
#                    name_transform=lambda name: name.upper(),
#                ),
#                #widget.TextBox(subprocess.check_output(["/bin/bash","-c","uname -r"]).decode('utf-8'), name="default"),
#                widget.Systray(),
#                #widget.Clock(format='%Y-%m-%d %a %I:%M %p'),
#                widget.Clock(format="[%m/%d/%Y %I:%M %p]")
#            ],
#            24,
#            background=colorpal["backgroundalt"],
#            opacity=0.88
#        )
    )
]

keys = keys

# Drag floating layouts.
mouse = [
    Drag([modKey], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([modKey], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([modKey], "Button2", lazy.window.bring_to_front())
]

floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
])
main = None
dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = True
cursor_warp = False
auto_fullscreen = False
focus_on_window_activation = "never"
reconfigure_screens = True
auto_minimize = False
wmname = "LG3D"
