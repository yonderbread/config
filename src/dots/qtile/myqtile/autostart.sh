#!/bin/bash
#export QT_STYLE_OVERRIDE=gtk2
#export QT_QPA_PLATFORMTHEME=qt5ct
pkill -9 lxsession && pkill pipewire && pkill pipewire-pulse && pkill pipewire-media-session && pkill -9 lemonbar && pkill -9 picom && pkill -9 xclip
xrandr --output DVI-D-0 --rate 60 --mode 1920x1080 --right-of HDMI-0
xrandr --output HDMI-0 --rate 144 --mode 2560x1440 --left-of DVI-D-0 --primary
xrdb -merge ~/.Xresources
#lxqt-session &
#gnome-session &
lxsession --noautostart --de=XFCE --session=default &
#mate-session &
#/usr/bin/pipewire & /usr/bin/pipewire-pulse & /usr/bin/pipewire-media-session &
cd ~/.config/lemonbar && ./run.sh &
pulseaudio -D -k --start &
nitrogen --restore &
picom --config ~/.picom.conf &
xclip &
dunst &
echo "Qtile startup finished :)"
