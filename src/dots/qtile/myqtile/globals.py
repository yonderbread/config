import json
import os

modKey = "mod4"

# Application preferences
myTerminal = "alacritty"
myEditor = "nvim"
#myBrowser = "librewolf"
myBrowser = "qutebrowser"
myPlayer = "mpv"
myFilemanager = "pcmanfm"

FONT_PRIMARY = {
    "font": 'Hack Nerd Font Regular',
    "fontsize": 14,
    "padding": 3
}

colorpal = json.load(open(os.path.expanduser("~/.config/qtile/myqtile/colors.json")))

layout_theme = {
    "border_width": 2,
    "margin": 20,
    "border_focus": colorpal["foregroundalt"],
    "border_normal": colorpal["accent1"]
}
