from libqtile.lazy import lazy
from libqtile.config import Key
from libqtile.backend.x11.xkeysyms import keysyms
import os
from .globals import (
    modKey, myTerminal, 
    myBrowser, myPlayer,
    myEditor,
    myFilemanager)

class KeysHolder:
    def __init__(self):
        for key in keysyms.keys():
            skey = key.upper()
            if key[0] in range(0, 9):
                skey = "K" + skey
            setattr(self, skey, key)

        self.ALT = self.MOD1 = "mod1"
        self.HYPER = self.MOD3 = "mod3"
        self.SUPER = self.MOD4 = "mod4"
        self.SHIFT = "shift"
        self.CONTROL = "control"
        self.EXCLAMATION = self.EXCLAM
        self.DOUBLE_QUOTE = self.QUOTEDBL

class MouseButtons:
    LEFT = BUTTON1 = "Button1"
    MIDDLE = BUTTON2 = "Button2"
    RIGHT = BUTTON3 = "Button3"
    WHEEL_UP = BUTTON4 = "Button4"
    WHEEL_DOWN = BUTTON5 = "Button5"
    WHEEL_LEFT = BUTTON6 = "Button6"
    WHEEL_RIGHT = BUTTON7 = "Button7"
    PREVIOUS = BUTTON8 = "Button8"
    NEXT = BUTTON9 = "Button9"

kh = KeysHolder()
mb = MouseButtons()

keys = [
    # Switch between windows
    Key([modKey], "h",
        lazy.layout.left(),
        desc="Move focus left"),
    Key([modKey], "Left",
        lazy.layout.left(),
        desc="Move focus left"),
    Key([modKey], "l",
        lazy.layout.right(),
        desc="Move focus right"),
    Key([modKey], "Right",
        lazy.layout.right(),
        desc="Move focus right"),
    Key([modKey], "j",
        lazy.layout.down(),
        desc="Move focus down"),
    Key([modKey], "Down",
        lazy.layout.down(),
        desc="Move focus down"),
    Key([modKey], "k",
        lazy.layout.up(),
        desc="Move focus up"),
    Key([modKey], "Up",
        lazy.layout.up(),
        desc="Move focus up"),
    Key([modKey], "space",
        lazy.layout.next(),
        desc="Focus next window"),
    Key([modKey, "shift"], "i",
        lazy.window.toggle_floating(),
        desc="Toggle floating"),
    Key([modKey, "shift"], "o",
        lazy.window.toggle_fullscreen(),
        desc="Toggle fullscreen"),
    Key([modKey, "shift"], "p",
        lazy.window.toggle_minimize(),
        desc="Toggle minimize"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([modKey, kh.SHIFT], "h",
        lazy.layout.shuffle_left(),
        desc="Move window left"),
    Key([modKey, kh.SHIFT], "l",
        lazy.layout.shuffle_right(),
        desc="Move window right"),
    Key([modKey, kh.SHIFT], "j",
        lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([modKey, kh.SHIFT], "k",
        lazy.layout.shuffle_up(),
        desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([modKey, kh.CONTROL], "h", 
        lazy.layout.grow_left(),
        desc="Extend window left"),
    Key([modKey, kh.CONTROL], "l", 
        lazy.layout.grow_right(),
        desc="Extend window right"),
    Key([modKey, kh.CONTROL], "j", 
        lazy.layout.grow_down(),
        desc="Extend window down"),
    Key([modKey, kh.CONTROL], "k",
        lazy.layout.grow_up(),
        desc="Extend window up"),
    Key([modKey], "n",
        lazy.layout.normalize(),
        desc="Normalize all windows"),

    # Toggle between split and unsplit sides of stack.
    Key([modKey, kh.SHIFT], "Return",
        lazy.layout.toggle_split(),
        desc="Toggle window stack split"),
    # Launch software
    Key([modKey], "Return",
        lazy.spawn(myTerminal),
        desc="Launch terminal"),
    Key([modKey, kh.ALT], "Return",
        lazy.spawn(myFilemanager)),
    Key([modKey], "m",
        lazy.spawn("mousemode"),
        desc="Launch mousemode, quit with Q"),
    Key([modKey], "b",
        lazy.spawn(myBrowser),
        desc="Launch browser"),
    Key([modKey, kh.SHIFT], "n",
        lazy.spawn("nvim-qt ~/devel"),
        desc="Launch Neovim QT"),
   Key([modKey, kh.SHIFT], "s",
        lazy.spawn("flameshot gui"),
        desc="Launch flameshot"), 
    Key([modKey], "s",
        lazy.spawn(".config/qtile/myqtile/rofi-beats"),
        desc="Spawn rofi-beats"),
    #Key([modKey], "m",
    #    lazy.spawn(myPlayer),
    #    desc="Launch media player"),
    # Toggle between different layouts as defined below
    Key([modKey], "Tab",
        lazy.next_layout(),
        desc="Cycle layouts"),
    Key([modKey], "q",
        lazy.window.kill(),
        desc="Kill focused window"),

    Key([modKey, kh.ALT], "l",
        lazy.spawn('i3lock-fancy-dualmonitor'),
        desc="Lock screen"),
    Key([modKey, kh.CONTROL], "r",
        lazy.restart(),
        desc="Restart Qtile"),
    Key([modKey, kh.CONTROL], "q",
        lazy.shutdown(),
        desc="Exit Qtile"),
    Key([modKey], "d",
        lazy.spawn('dmenu_run'),
        desc="Launch dmenu"),
    Key([modKey], "x",
        lazy.spawn('rofi -upgrade-config -show drun -modi drun -show-icons -theme ~/.config/rofi/themes/Arc-Dark.rasi -show-theme -icon-theme "Papirus"'),
        desc="Launch rofi"),
    Key([modKey, kh.SHIFT], "x",
        lazy.spawn('.config/qtile/myqtile/rofi-bluetooth'),
        desc="Launch bluetooth menu")
]

def dump_keybinds_to_file():
    text = ""
    for b in keys:
        kb = ("-".join(b.modifiers)).upper()
        if b.key:
            kb += "-" + b.key
        text += f"{kb}\n"
        text += f"{b.desc}\n"
    with open(os.path.expanduser("~/.qtile_keybinds.txt"), "w") as f:
        f.flush()
        f.write(text)
        f.close()
    lazy.spawn("alacritty --command nvim ~/.qtile_keybinds.txt")

keys.append(Key([modKey, kh.SHIFT], "y",
    lazy.function(dump_keybinds_to_file()),
    desc="Dump keybinds to text file in home directory"))
