" Basic vim settings
set noswapfile
set nobackup
set encoding=UTF-8
set showmatch
set hlsearch
set expandtab
set shiftwidth=2
set autoindent
set number
set ruler
set cursorline
set mouse
syntax enable
filetype plugin indent on
filetype plugin on

" Plugins
call plug#begin(stdpath('config') . '/plugged')

Plug 'yonderbread/vim-pigments'
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
"Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }    
"Plug 'zchee/deoplete-jedi'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'jiangmiao/auto-pairs'
Plug 'scrooloose/nerdcommenter'
Plug 'sbdchd/neoformat'
"Plug 'davidhalter/jedi-vim'
Plug 'scrooloose/nerdtree'
Plug 'neomake/neomake'
Plug 'terryma/vim-multiple-cursors'
Plug 'machakann/vim-highlightedyank'
"Plug 'tmhedberg/SimpylFold'
Plug 'morhetz/gruvbox'
Plug 'cocopon/iceberg.vim'
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}
Plug 'kvrohit/substrata.nvim'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'romgrk/barbar.nvim'
Plug 'folke/trouble.nvim'
Plug 'editorconfig/editorconfig-vim'
Plug 'jacoborus/tender.vim'
Plug 'folke/tokyonight.nvim', {'branch':'main'}
Plug 'Rigellute/rigel'
Plug 'pangloss/vim-javascript'
Plug 'Rigellute/rigel'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'folke/which-key.nvim'
Plug 'folke/todo-comments.nvim'
Plug 'glepnir/dashboard-nvim'

call plug#end()

" glepnir/dashboard-nvim
let g:dashboard_default_executive = 'telescope'
let g:dashboard_custom_header=<< trim END
          ⠀⠀⠀⠀⠀  ⠀⠀⠀⠀⠀⠀⢀⡀⠀⠀⠀⠀⢀⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
          ⠀⠀⠀⠀⠀  ⠀⠀⠀⠀⣠⡖⠁⠀⠀⠀⠀⠀⠀⠈⢲⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀
          ⠀⠀⠀⠀⠀  ⠀⠀⠀⣼⡏⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢹⣧⠀⠀⠀⠀⠀⠀⠀⠀
          ⠀⠀⠀⠀⠀  ⠀⠀⣸⣿⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⣿⣇⠀⠀⠀⠀⠀⠀⠀
          ⠀⠀⠀⠀⠀⠀  ⠀⣿⣿⡇⠀⢀⣀⣤⣤⣤⣤⣀⡀⠀⢸⣿⣿⠀⠀⠀⠀⠀⠀⠀
          ⠀⠀⠀⠀⠀⠀  ⠀⢻⣿⣿⣔⢿⡿⠟⠛⠛⠻⢿⡿⣢⣿⣿⡟⠀⠀⠀⠀⠀⠀⠀
          ⠀⠀⠀⠀  ⣀⣤⣶⣾⣿⣿⣿⣷⣤⣀⡀⢀⣀⣤⣾⣿⣿⣿⣷⣶⣤⡀⠀⠀⠀⠀
          ⠀⠀  ⢠⣾⣿⡿⠿⠿⠿⣿⣿⣿⣿⡿⠏⠻⢿⣿⣿⣿⣿⠿⠿⠿⢿⣿⣷⡀⠀⠀
            ⠀⢠⡿⠋⠁⠀⠀⢸⣿⡇⠉⠻⣿⠇⠀⠀⠸⣿⡿⠋⢰⣿⡇⠀⠀⠈⠙⢿⡄⠀
            ⠀⡿⠁⠀⠀⠀⠀⠘⣿⣷⡀⠀⠰⣿⣶⣶⣿⡎⠀⢀⣾⣿⠇⠀⠀⠀⠀⠈⢿⠀
            ⠀⡇⠀⠀⠀⠀⠀⠀⠹⣿⣷⣄⠀⣿⣿⣿⣿⠀⣠⣾⣿⠏⠀⠀⠀⠀⠀⠀⢸⠀
            ⠀⠁⠀⠀⠀⠀⠀⠀⠀⠈⠻⢿⢇⣿⣿⣿⣿⡸⣿⠟⠁⠀⠀⠀⠀⠀⠀⠀⠈⠀
            ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣼⣿⣿⣿⣿⣧⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
            ⠀⠀⠀⠐⢤⣀⣀⢀⣀⣠⣴⣿⣿⠿⠋⠙⠿⣿⣿⣦⣄⣀⠀⠀⣀⡠⠂⠀⠀⠀
            ⠀⠀⠀⠀⠀⠈⠉⠛⠛⠛⠛⠉⠀⠀⠀⠀⠀⠈⠉⠛⠛⠛⠛⠋⠁⠀⠀⠀⠀⠀
END

colorscheme rigel
"colorscheme tokyonight
"colorscheme substrata
"colorscheme gruvbox
"set background=dark
"set background=light

" folke/tokyonight settings
let g:tokyonight_style = "night"
let g:tokyonight_transparent = 0.97
let g:tokyonight_transparent_sidebar = 1
let g:tokyonight_dark_sidebar = 1
let g:tokyonight_italic_functions = 1

" Rigellute/rigel settings
let g:rigel_airline = 1
let g:javascript_plugin_flow = 1

let g:airline_theme='rigel'
"let g:airline_theme='ayu_dark'
"let g:airline_theme='desertink' 
" Enable alignment
let g:neoformat_basic_format_align = 1
" Enable tab to space conversion
let g:neoformat_basic_format_retab = 1
" Enable trimmming of trailing whitespace
let g:neoformat_basic_format_trim = 1
"close the autcomplete preview window automatically
" disable autocompletion, because we use deoplete for completion
let g:jedi#completions_enabled = 0
" open the go-to function in split, not another buffer
let g:jedi#use_splits_not_buffers = "right"
" set pylint as python linter
let g:neomake_python_enabled_makers = ['pylint']
" fix yank colors
hi HighlightedyankRegion cterm=reverse gui=reverse
" increase yank highlight duration in ms
let g:highlightedyank_highlight_duration = 1000
" Set up powerline fonts for airline
let g:airline_powerline_fonts     = 1
if !exists('g:airline_symbols')
    let g:airline_symbols         = {}
endif
let g:airline_symbols.maxlinenr   = ' '
let g:airline_symbols.linenr      = ' row:'
let g:airline_symbols.colnr       = 'col:'

autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif
" navigate autocomplete list with TAB
inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"
" Turn on 256 bit colors
set termguicolors

" iamcco/markdown-preview.nvim
" COMMAND REFERENCE:
" :MarkdownPreview - Start preview
" :MarkdownPreviewStop - Stop preview
let g:mkdp_auto_start             = 1
let g:mkdp_auto_close             = 1
let g:mkdp_refresh_slow           = 0
let g:mkdp_command_for_global     = 0
let g:mkdp_open_to_the_world      = 0
let g:mkdp_preview_options        = {
    \ 'mkit': {},
    \ 'katex': {},
    \ 'uml': {},
    \ 'maid': {},
    \ 'disable_sync_scroll': 0,
    \ 'sync_scroll_type': 'middle',
    \ 'hide_yaml_meta': 0,
    \ 'sequence_diagrams': {},
    \ 'flowchart_diagrams': {},
    \ 'content_editable': v:true,
    \ 'disable_filename': 0
    \ }
let g:mkdp_page_title             = '「${name}」'
let g:mkdp_filetypes              = ['markdown']
"let g:mkdp_markdown_css          = expand('~/.config/nvim/markdown.css')
"let g:mkdp_highlight_css         = ''
"let g:mkdp_port                  = ''
"let g:mkdp_open_ip               = ''
"let g:mkdp_browser               = ''
"let g:mkdp_echo_preview_url      = 0
"let g:mkdp_browserfunc           = ''

" Setup folke/trouble.nvim
lua << EOF
    require("trouble").setup 
    {
        position = "bottom",
        height           = 8,
        width            = 65,
        icons            = true,
        mode             = "lsp_workspace_diagnostics",
        action_keys = {
            close             = "q",
            cancel            = "<esc>",
            refresh           = "r",
            jump              = {"<cr>", "<tab>"},
            open_split        = {"<c-x>"},
            open_vsplit       = {"c-v"},
            open_tab          = {"<c-t>"},
            jump_close        = {"o"},
            toggle_mode       = "m",
            toggle_preview    = "p"
                    
        }
    }
EOF
