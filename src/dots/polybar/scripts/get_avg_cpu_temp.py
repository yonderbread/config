#!/usr/bin/python
import subprocess, sys, json

# Get and parse json sensors output
def get_sensors_output(sensor_name="coretemp-isa-0000"):
    opr = subprocess.check_output(f"sensors -j {sensor_name}", shell=True).decode('utf-8').strip()
    op = json.loads(opr)
    return op

# Get average integer value from array of integers
def get_avg(vals=[]):
    total = 0
    for i in vals:
        total += i
    return total / len(vals)

# Get values
def get_temps(op: dict, sensor_name="coretemp-isa-0000"):
    vals = []
    i = 1
    for k, v in op[sensor_name].items():
        #print(k, v)
        if type(v) != dict:
            continue
        for k_ in v.keys():
            if "_input" in k_:
                vals.append(v[f"temp{str(i)}_input"])
                i += 1
    return vals

if __name__ == '__main__':
    op = get_sensors_output()
    temps = get_temps(op)
    sys.stdout.write(str(get_avg(temps)))

