#!/usr/bin/env bash
set -e
sudo pacman -Sy --noconfirm git cmake make gcc ninja aria2
#git clone --recurse-submodules --jobs=5 https://github.com/wxWidgets/wxWidgets.git wxwidgets
aria2c -x 16 -s 64 -c --max-split-size=1M \
    --max-tries=8 --max-file-not-found=8 \
    --allow-overwrite \
    "https://github.com/wxWidgets/wxWidgets/releases/download/v3.1.5/wxWidgets-3.1.5.zip" \
    --out wxwidgets.zip
unzip wxwidgets.zip -d wxwidgets-git
rm wxwidgets.zip
cd wxwidgets-git \
    && mkdir -p ./buildgtk \
    && cd buildgtk \
    && ../configure --with-gtk \
    && make -j$(nproc) \
    && sudo make install \
    && sudo ldconfig && cd ../../
paru -Sy --noconfirm --cleanafter wxgtk3-dev-light
sudo pacman -S --noconfirm cmake ninja ccache expat \
    gcc-libs gdk-pixbuf2 glibc flac gtk3 glib2 libid3tag \
    lilv libmad libogg portaudio portmidi libsndfile \
    libsoxr suil twolame vamp-plugin-sdk libvorbis soundtouch \
    ffmpeg
export WX_CONFIG=/usr/bin/wx-config-gtk3

# Now finally build tenacity
git clone https://github.com/tenacityteam/tenacity.git tenacity-git
cd tenacity-git && ./configure && \
    cmake -G Ninja -S . -B build \
    cmake --build build &&
    sudo cmake --install build
cd .. && rm -rf tenacity-git && rm -rf wxwidgets-git
echo COMPLETED.

