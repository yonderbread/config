#!/usr/bin/env
set -e
tempdir=$HOME/_tmp
mkdir -p $tempdir && cd $tempdir

git clone --depth=1 --branch=master https://github.com/Anuken/Mindustry
cd Mindustry

sed -i 's/applicationId "io.anuke.mindustry"/applicationId "io.anuke.mindustry.be"/g' android/build.gradle
sed -i 's/"io.anuke.mindustry"/"io.anuke.mindustry.be"/g' android/AndroidManifest.xml
sed -i 's/Mindustry/Mindustry BE/g' android/res/values/strings.xml

./gradlew pack
./gradlew desktop:dist server:dist android:assembleRelease

