#!/usr/bin/env bash
set -e
sudo apt update
sudo apt install -y git clang libboost-fiber-dev libboost-context-dev libboost-thread-dev libboost-system-dev libx11-dev libxtst-dev
git clone https://gitlab.com/yonderbread/mouser mouser-git
cd mouser-git && make
