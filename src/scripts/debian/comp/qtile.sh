#!/usr/bin/env bash
set -e
sudo apt update
sudo apt install -y git
sudo apt install -y python3-cffi python3-xcffib python3-cairocffi \
    libwlroots-dev libpangocairo-1.0-0 xserver-xorg python-is-python3 \
    python3-pip
python -m pip install --upgrade pip
pip install --no-cache-dir cairocffi
python -m pip install --force-reinstall pywlroots pywayland python-xkbcommon dbus-next
git clone https://github.com/qtile/qtile
cd qtile
pip install .
