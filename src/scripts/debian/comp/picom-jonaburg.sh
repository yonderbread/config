#!/usr/bin/env bash
set -e
sudo apt install -y meson libx11-dev libx11-xcb-dev libxext-dev xcb-proto xcb libxcb-damage0-dev libxcb-xfixes0-dev libxcb-shape0-dev libxcb-render-util0-dev libxcb-render0-dev libxcb-randr0-dev libxcb-composite0-dev libxcb-image0-dev libxcb-present-dev libxcb-xinerama0-dev libxcb-glx0-dev libpixman-1-dev libdbus-1-dev libconfig-dev libgl-dev libpcre3-dev libev-dev uthash-dev git
git clone https://github.com/jonaburg/picom picom-junaburg
cd picom-junaburg && git submodule update --init --recursive
meson --buildtype=release . build
ninja -C build
sudo ninja -C build install
