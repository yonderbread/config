#!/usr/bin/env bash

set -e

# Install deps and required programs
sudo apt install -y libx11-dev libxinerama-dev libxext-dev libxrandr-dev libxss-dev \
    git curl haskell-stack
# Install haskell stack
#curl -sSL https://get.haskellstack.org/ | sh
# Backup xmonad config directory
if [[ -d "~/.config/xmonad" ]]
then
    mv ~/.config/xmonad ~/.config/xmonad_backup
fi
# Create xmonad config directory and enter it
mkdir -p ~/.config/xmonad && cd ~/.config/xmonad
# Create basic xmonad.hs
echo "import Xmonad" > xmonad.hs
echo "main :: IO()" >> xmonad.hs
echo "main = xmonad def" >> xmonad.hs
# Clone repos
git clone -b v0.15 https://github.com/xmonad/xmonad
git clone -b v0.16 https://github.com/xmonad/xmonad-contrib
# Initialize stack & install stack requirements
stack init && stack install
# Add ~/.local/bin to PATH variable in ~/.profile
# if it hasn't already been added
if [ -d "$HOME/.local/bin" ] && [[ ":$PATH" != *":$HOME/.local/bin:"* ]]; then
    echo "PATH=\$PATH:\$HOME/.local/bin" >> ~/.profile
fi
# Add ~/.local/bin to path now so we don't have to
# run source ~/.profile
PATH="$PATH:$HOME/.local/bin"
# Create basic xmonad build script
echo "#!/bin/bash" >> build
echo "exec stack ghc -- \\" >> build
echo "  --make xmonad.hs \\" >> build
echo "  -i \\" >> build
echo "  -ilib \\" >> build
echo "  -fforce-recomp \\" >> build
echo "  -main-is main \\" >> build
echo "  -v0 \\" >> build
echo "  -o \"\$1\"" >> build
# Make build script executable
chmod +x build



