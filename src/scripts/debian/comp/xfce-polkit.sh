#!/usr/bin/env bash
set -e
sudo apt install -y git libglib2.0-dev libxfce4ui-2-dev ninja-build libpolkit-agent-1-dev libpolkit-gobject-1-dev libpolkit-agent-1-dev libpolkit-qt5-1-dev build-essential meson
git clone https://gitlab.com/yonderbread/xfce-polkit xfce-polkit-git
cd xfce-polkit-git
mkdir build && cd build
meson --prefix=/usr ..
ninja
sudo ninja install
