#!/bin/sh

sudo apt update
sudo apt -y install build-essential scons pkg-config libx11-dev libxcursor-dev libxinerama-dev libgl1-mesa-dev libglu-dev libasound2-dev libpulse-dev libudev-dev libxi-dev libxrandr-dev yasm

git clone https://github.com/godotengine/godot.git godot
cd godot && scons -j$(nproc) platform=x11
