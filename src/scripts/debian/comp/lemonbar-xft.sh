#!/usr/bin/env bash
set -e
sudo apt update && sudo apt install -y git make cmake build-essential
git clone https://gitlab.com/protesilaos/lemonbar-xft.git lemonbar-xft
cd lemonbar-xft && make
