#!/usr/bin/env bash
set -e
sudo apt update && sudo apt install -y git libqt5dbus5 libqt5charts5-dev libqt5widgets5 libqt5network5 libqt5svg5-dev \
    extra-cmake-modules qtdeclarative5-dev libkf5filemetadata-dev libkf5archive-dev libkf5archive5 libamd2 libdrm-dev \
    libdrm-radeon1 libdrm-amdgpu1 libdrm-common libdrm-nouveau2 libdrm-intel1 
git clone https://gitlab.com/corectrl/corectrl.git corectrl
cd corectrl && mkdir build && cd build
cmake -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTING=OFF ..
make -j$(nproc)
sudo make install
