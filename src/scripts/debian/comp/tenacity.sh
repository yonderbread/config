#!/usr/bin/env bash
set -e
sudo apt update
sudo apt install -y git cmake build-essential ffmpeg python-is-python3 python3-pip \
    libgtk2.0-dev libasound2-dev libavformat-dev libjack-jackd2-dev uuid-dev zlib1g-dev \
    zlib1g libportaudio2 libportaudiocpp0 nyquist sqlite3 libsmbclient-dev
sudo python -m pip install conan
git clone https://github.com/tenacityteam/tenacity tenacity-git
cd tenacity-git && mkdir build && cd build && cmake -G "Unix Makefiles" -Duse_ffmpeg=loaded .. && \
    make -j$(nproc)
