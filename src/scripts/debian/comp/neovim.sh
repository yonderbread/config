#!/usr/bin/env bash
sudo apt-get install ninja-build gettext libtool libtool-bin autoconf automake cmake g++ pkg-config unzip git -y
git clone https://github.com/neovim/neovim neovim
cd neovim && make -j$(nproc)
