#!/bin/bash

# Temporary enviorment variables
ARDOUR_BUILD_VER=6.7
ARDOUR_BUILD_REPO_DIRNAME=ardour_daw

# Dependencies
sudo apt update
sudo apt install -y git build-essential cairo-dock-dev libgtk2.0-dev \
    libatk1.0-dev libjack-dev libalsaplayer-dev libsndfile1-dev libvorbis-dev \
    libmad0-dev libsamplerate0-dev librubberband-dev liblo-dev \
    lv2-dev liblilv-dev libsuil-dev libaubio-dev libcurl4 \
    curl cmake libboost-dev gettext libgtk2.0-doc intltool \
    libffi-dev libpng-dev libsigc++-2.0-dev libtool libxml2-dev \
    m4 libnss3-dev pkg-config libpango1.0-dev libpcre3-dev libraptor2-dev \
    readline-common libreadline-dev libsord-dev libtar-dev libtiff-dev uuid-dev \
    xz-utils libatkmm-1.6-dev autoconf bison libbison-dev libcairo2-dev libcairomm-1.0-dev \
    libcppunit-dev libexpat1-dev libflac-dev libfontconfig1-dev libglib2.0-dev gnome-common \
    gobject-introspection libgtkmm-3.0-dev libharfbuzz-dev itstool libarchive-dev \
    libiconv-hook-dev libogg-dev libusb-dev libwebsockets-dev libxslt1-dev liblrdf0-dev \
    make nss-plugin-pem libpangomm-1.4-dev libpixman-1-dev libportaudio-ocaml-dev \
    librasqal3-dev redland-utils libserd-dev libsratom-dev libtaglib-ocaml-dev util-linux \
    vamp-plugin-sdk zlib1g-dev

# Clone repo
git clone https://github.com/Ardour/ardour.git $ARDOUR_BUILD_REPO_DIRNAME
# Switch branch to desired tag/version
cd $ARDOUR_BUILD_REPO_DIRNAME && git checkout $ARDOUR_BUILD_VER

# Build
./waf clean
./waf configure
./waf
